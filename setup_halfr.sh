#!/bin/bash -e

echo "[!] Getting private key"
mkdir -p $HOME/.ssh
scp -P 2209 "halfr.net:.ssh/id_rsa" $HOME/.ssh/

echo "[!] Cloning config repository"
hg clone ssh://hg@bitbucket.org/halfr/configs $HOME/configs

echo "[!] Running configs scripts"
cd $HOME/configs
./setup_workflow.sh
cd -
