#!/bin/zsh

emulate -L zsh # pure zsh, no emulation
set -e # die with errors

zmodload zsh/zutil

SCRIPT_NAME=$0

VBOX=VBoxManage

help () {
    echo "Usage: $SCRIPT_NAME --name NAME --os OS_TYPE[OPTIONS]"
    echo
    echo "Create a VirtualBox VM."
    echo
    echo "Options:"
    echo "  -n, --name=NAME\t\tset the VM to NAME"
    echo "  -o, --os=OS_TYPE\t\tset ostype to OS_TYPE " \
        "(see VBoxManage list ostypes)"
    echo "  -b, --basefolder=DIR\t\tset the base folder for the VM to DIR"
    echo
    echo "Optional arguments:"
    echo "  -s, --size=SIZE\t\tattach a disk with a size of SIZE Mio" \
        "(default 4096 Mio)"
    echo "  -i, --iso=FILE\t\tload the VM with FILE attached"
    echo "  -m, --memory=SIZE\t\tset the VM memory size to SIZE Mio" \
        "(default 128 Mio)"
    echo "  -S, --start\t\t\tstart the VM"

    exit 1
}

main () {
    [[ $# -eq 0 ]] && help && exit 1

    zparseopts -A args -a foo -M \
        h=opt_help -help=h \
        n:=opt_name -name:=n \
        o:=opt_os -os:=o \
        b:=opt_base -basefolder:=b \
        s:=opt_size -size:=s \
        i:=opt_iso -iso:=i \
        m:=opt_memory -memory:=m \
        S=opt_start -start=S

    [[ -n $opt_help ]] && help

    # sanity checks
    [[ -z $opt_name ]] || [[ -z $opt_base ]] \
        || [[ -z $opt_os ]] \
        || [[ -n $opt_size ]] && [[ ${args[-s]} != <-> ]] \
        || [[ -n $opt_memory ]] && [[ ${args[-m]} != <-> ]] && help


    # set variables
    VM=${args[-n]}
    base_folder=${args[-b]}
    hdd_size=${args[-s]:=4096}
    iso_file=${args[-i]}
    os_type=${args[-o]}
    memory_size=${args[-m]}
    start=$opt_start

    # display info
    echo "VM name: $VM"
    echo "Base folder: $base_folder"
    [[ -n $iso_file ]] && echo "Load ISO: $iso_file"
    echo "HDD size: $hdd_size Mio"
    [[ -n $memory_size ]] && echo "Memory size: $memory_size Mio"
    echo

    # start creating the VM
     $VBOX createhd --filename "$base_folder/$VM/$VM.vdi" --size $hdd_size
     $VBOX createvm --name "$VM" --ostype "$os_type" \
         --basefolder "$base_folder" --register
     $VBOX storagectl "$VM" --name "SATA Controller" --add sata
     $VBOX storageattach "$VM" --storagectl "SATA Controller" --port 0 \
        --device 0 --type hdd --medium "$base_folder/$VM/$VM.vdi"

    if [[ -n $iso_file ]]; then
         $VBOX storageattach "$VM" --storagectl "SATA Controller" --port 1 \
            --device 0 --type dvddrive --medium "$iso_file"
    fi

    if [[ -n $memory_size ]]; then
         $VBOX modifyvm "$VM" --memory $memory_size
    fi

    if [[ -n $start ]]; then
        exec $VBOX startvm "$VM" --type sdl
    fi
}

main $*
