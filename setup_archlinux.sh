#!/bin/bash

set -e

echo "[!] Setup Archlinux"
echo "[!] Updated for release 2012.12.xx"
echo
echo "[!] Installing base tools"

echo
echo "[!] Set LIGHT_SETUP=1 to install less packages"
echo "[!] Hit ctrl-c if you want to change your mind"
echo -n .
sleep 1
echo -n .
sleep 1
echo .
sleep 1

echo
echo "[!] Go!"

echo "[!] Adding archlinuxfr repository (for yaourt)"

cat >> /etc/pacman.conf << EOF
[archlinuxfr]
Server = http://repo.archlinux.fr/\$arch
EOF

echo "[!] Installing yaourt"
pacman --noconfirm -Sy yaourt

echo "[!] Installing packages"
yaourt --noconfirm -S $(<base_packages.list)

[[ -n $LIGHT_SETUP ]] || yaourt --noconfirm -S $(<more_packages.list)

echo "[!] Creating user halfr"
useradd halfr -g users -s /bin/zsh -m
echo "[!] Setting password for user halfr"
passwd halfr

echo "[!] Downloading halfr install script"
cd /home/halfr
wget -c http://halfr.net/scripts/{setup_halfr.sh,base_packages.list,more_packages.list}
chown -v halfr:users setup_halfr.sh
chmod +x setup_halfr.sh

echo "[!] You can now run setup_halfr.sh"
su -l halfr
