#!/bin/zsh
# require youtube-dl
# $ ./youtube.zsh start
# $ ./youtube.zsh clean

emulate -L zsh

setopt extendedglob
setopt nullglob

say() {
    notify-send -t 1000 DL $1
}

download() {
    {
        say "Download of "$1" started"
        youtube-dl -t -q -c "$1"
        say "Download of "$1" finished"
    } &
}

run() {
    last=""
    while sleep 1; do
        new="$(xclip -o)"
        if [[ $new != $last ]]; then
            last="$new"
            [[ "$new" =~ youtu ]] && download "$new"
        fi
    done
}

case $1 in
    clean)
        echo clean
        rm -vf *.mp4
        exit 0
        ;;
    start)
        run
        ;;
    *)
        echo "Usage: $0 [start|clean]"
        exit 1
        ;;
esac

