#!/usr/bin/perl
# Author: Todd Larason <jtl@molehill.org>
# Modified by: Rémi Audebert <mail@halfr.net>

# use the resources for colors 0-15 - usually more-or-less a
# reproduction of the standard ANSI colors, but possibly more
# pleasing shades

# colors 16-231 are a 6x6x6 color cube
for ($red = 0; $red < 6; $red++) {
    for ($green = 0; $green < 6; $green++) {
        for ($blue = 0; $blue < 6; $blue++) {
            printf("\x1b]4;%d;rgb:%2.2x/%2.2x/%2.2x\x1b\\",
                16 + ($red * 36) + ($green * 6) + $blue,
                ($red ? ($red * 40 + 55) : 0),
                ($green ? ($green * 40 + 55) : 0),
                ($blue ? ($blue * 40 + 55) : 0));
        }
    }
}

# colors 232-255 are a grayscale ramp, intentionally leaving out
# black and white
for ($gray = 0; $gray < 24; $gray++) {
    $level = ($gray * 10) + 8;
    printf("\x1b]4;%d;rgb:%2.2x/%2.2x/%2.2x\x1b\\",
        232 + $gray, $level, $level, $level);
}

# display the colors

# first the system ones:
print "System colors:\n";

print "0 ";
for ($color = 0; $color < 8; $color++) {
    print "\x1b[48;5;${color}m  ";
}
print "\x1b[0m";
print " 7\n";

print "8 ";
for ($color = 8; $color < 16; $color++) {
    print "\x1b[48;5;${color}m  ";
}
print "\x1b[0m";
print " 15\n\n";

# now the color cube
print "Color cubes, colorcode = 16 + red * 36 + green * 6 + blue:\n";
for ($green = 0; $green < 6; $green++) {
    print "         Green: ${green}\n";
    for ($red = 0; $red < 6; $red++) {
        print "Red: ${red} ";
        for ($blue = 0; $blue < 6; $blue++) {
            $color = 16 + ($red * 36) + ($green * 6) + $blue;
            print "\x1b[48;5;${color}m  ";
        }

        print "\x1b[0m\n";
    }
    print "Blue:  0 1 2 3 4 5 \n";
    print "\n";
}

# now the grayscale ramp
print "Grayscale ramp:\n";
print "232 ";
for ($color = 232; $color < 256; $color++) {
    print "\x1b[48;5;${color}m  ";
}
print "\x1b[0m";
print " 255\n";
